# Copyright (c) 2023 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration tests for trees"""
from tests.test_integration import IntegrationTests
from tests.test_integration import assets_mkdir
from tests.test_integration import kpet_tree_list


class IntegrationTreesTests(IntegrationTests):
    """Tests for trees"""

    def test_list(self):
        """
        Check the "kpet tree list" command works appropriately.
        """
        assets = {
            "index.yaml": """
                template: output.txt.j2
                arches:
                  - a
                  - b
                  - c
                trees:
                  a:
                    arches: a
                  b:
                    arches: b
                  a_and_b:
                    arches: a|b
                host_types:
                  normal: {}
                case:
                  universal_id: test
                  name: test
                  location: somewhere
                  maintainers:
                    - name: maint
                      email: maint@maintainers.org
            """,
            "output.txt.j2": ""
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_tree_list, db_path,
                stdout_equals="a\na_and_b\nb\n"
            )

            self.assertKpetProduces(
                kpet_tree_list, db_path, "-a.*",
                stdout_equals="a\na_and_b\nb\n"
            )

            self.assertKpetProduces(
                kpet_tree_list, db_path, "--arch=.*",
                stdout_equals="a\na_and_b\nb\n"
            )

            self.assertKpetProduces(
                kpet_tree_list, db_path, "-a.*", ".*",
                stdout_equals="a\na_and_b\nb\n"
            )

            self.assertKpetProduces(
                kpet_tree_list, db_path, ".*",
                stdout_equals="a\na_and_b\nb\n"
            )

            self.assertKpetProduces(
                kpet_tree_list, db_path, "-a.*", "",
                stdout_equals=""
            )

            self.assertKpetProduces(
                kpet_tree_list, db_path, "-aa",
                stdout_equals="a\na_and_b\n"
            )

            self.assertKpetProduces(
                kpet_tree_list, db_path, "-aa", "b",
                stdout_equals=""
            )

            self.assertKpetProduces(
                kpet_tree_list, db_path, "-ab",
                stdout_equals="a_and_b\nb\n"
            )

            self.assertKpetProduces(
                kpet_tree_list, db_path, "-aa|b",
                stdout_equals="a\na_and_b\nb\n"
            )

            self.assertKpetProduces(
                kpet_tree_list, db_path, "-ac",
                stdout_equals=""
            )

            self.assertKpetProduces(
                kpet_tree_list, db_path, "-aa|b", "a_and_b",
                stdout_equals="a_and_b\n"
            )

            self.assertKpetProduces(
                kpet_tree_list, db_path, "-aa|b", "a",
                stdout_equals="a\n"
            )

            self.assertKpetProduces(
                kpet_tree_list, db_path, "a",
                stdout_equals="a\n"
            )
