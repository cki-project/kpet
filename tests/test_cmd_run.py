# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Test cases for run command module"""
from io import StringIO
import os
import unittest

import mock

from kpet import cmd_run
from kpet import data
from kpet import misc
from kpet import run


class CmdRunTest(unittest.TestCase):
    """Test cases for run command module."""

    def setUp(self):
        self.dbdir = os.path.join(os.path.dirname(__file__),
                                  'assets/db/general')

    def test_generate(self):
        """
        Check the success case.
        """
        self.maxDiff = None  # pylint: disable=invalid-name
        database = data.Base(self.dbdir)
        for tree in ("rhel7", "rhel8"):
            scenario = run.Scenario(database)
            target = data.Target(arches={'x86_64'}, trees={tree})
            scenario.add_scene(domain_paths=None, target=target, kernel='bar')
            variables = {
                n: i['default']
                for n, i in database.variables.items()
                if 'default' in i
            }
            content = scenario.generate(description='Foo',
                                        lint=True, variables=variables)
            with open(os.path.join(self.dbdir, tree + '_rendered.xml'),
                      encoding='utf8') as fhandle:
                content_expected = fhandle.read()
            self.assertEqual(content_expected, content)

    def test_main(self):
        """
        Check generate function is called and that ActionNotFound is
        raised when the action is not found.
        """
        mock_args = mock.Mock()
        mock_args.action = 'generate'
        mock_args.tree = 'rhel7'
        mock_args.kernel = 'kernel'
        mock_args.arch = 'arch'
        mock_args.db = self.dbdir
        mock_args.no_lint = None
        mock_args.output = None
        mock_args.cookies = None
        mock_args.description = 'description'
        mock_args.mboxes = []
        mock_args.file_list = None
        mock_args.action = 'action-not-found'
        self.assertRaises(misc.ActionNotFound, cmd_run.main, mock_args)

    def test_invalid_dbdir(self):
        """Check invalid dbdir raises an exception."""
        mock_args = mock.Mock()
        mock_args.action = 'generate'
        mock_args.tree = 'rhel7'
        mock_args.kernel = 'kernel'
        mock_args.arch = 'arch'
        mock_args.db = '/tmp/igotgoosebutnoshoes'
        mock_args.output = None
        mock_args.cookies = None
        mock_args.description = 'description'
        mock_args.mboxes = []
        mock_args.file_list = None

        with self.assertRaises(Exception):
            cmd_run.main(mock_args)

    @mock.patch('sys.stdout', new_callable=StringIO)
    def test_print_testcases(self, mock_stdout):
        """Check tests are listed for a run."""
        mock_args = mock.Mock()
        mock_args.action = 'test'
        mock_args.test_subaction = 'list'
        mock_args.tree = 'rhel7'
        mock_args.kernel = 'kernel'
        mock_args.arch = 'x86_64'
        mock_args.components = None
        mock_args.sets = None
        mock_args.tests = None
        mock_args.type = 'auto'
        mock_args.db = self.dbdir
        mock_args.output = None
        mock_args.cookies = None
        mock_args.description = 'description'
        mock_args.mboxes = None
        mock_args.file_list = None
        mock_args.global_mboxes = []
        mock_args.patches = []
        mock_args.domains = None
        mock_args.prev = None
        mock_args.targeted = False
        mock_args.triggered = False
        mock_args.files = []
        mock_args.high_cost = 'yes'

        cmd_run.main(mock_args)

        expected = ["/kernel/distribution/ltp/lite", "fs/ext4", "fs/xfs"]

        self.assertEqual(mock_stdout.getvalue().splitlines(), expected)

    def test_invalid_tree(self):
        """Check invalid tree raises an exception."""

        mock_args = mock.Mock()
        mock_args.action = 'generate'
        mock_args.tree = 'rhel0'
        mock_args.kernel = 'kernel'
        mock_args.arch = 'x86_64'
        mock_args.db = self.dbdir
        mock_args.output = None
        mock_args.cookies = None
        mock_args.description = 'description'
        mock_args.mboxes = []
        mock_args.file_list = None

        with self.assertRaises(Exception):
            cmd_run.main(mock_args)

    def test_invalid_arch(self):
        """Check invalid arch raises an exception."""

        mock_args = mock.Mock()
        mock_args.action = 'generate'
        mock_args.tree = 'rhel7'
        mock_args.kernel = 'kernel'
        mock_args.arch = 'foo'
        mock_args.db = self.dbdir
        mock_args.output = None
        mock_args.cookies = None
        mock_args.description = 'description'
        mock_args.mboxes = []
        mock_args.file_list = None
        mock_args.patches = []

        with self.assertRaises(Exception):
            cmd_run.main(mock_args)

    def _assert_standard_testcases(self, mock_stdout):

        self.assertEqual(
            mock_stdout.write.call_args_list[0],
            mock.call('/kernel/distribution/ltp/lite'),
        )
        self.assertEqual(
            mock_stdout.write.call_args_list[2],
            mock.call('fs/ext4'),
        )
        self.assertEqual(
            mock_stdout.write.call_args_list[4],
            mock.call('fs/xfs'),
        )
