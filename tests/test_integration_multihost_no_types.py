# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration multihost tests"""
from tests.test_integration import BEAKER_XML_J2
from tests.test_integration import INDEX_BASE_YAML
from tests.test_integration import IntegrationTests
from tests.test_integration import assets_mkdir


class IntegrationMultihostNoTypesTests(IntegrationTests):
    """Multihost integration tests with no type"""

    def test_multihost_no_types_no_regex_no_suites(self):
        """Test multihost support without types/regex/cases"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                    panicky:
                        ignore_panic: true
                    multihost_1: {}
                    multihost_2: {}
                recipesets:
                    rcs1:
                      - normal
                      - panicky
                    rcs2:
                      - multihost_1
                      - multihost_2
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetSrcMatchesNoneOfTwoCases(db_path)

    def test_multihost_no_types_no_regex_two_suites(self):
        """Test multihost support without types/regex and two cases"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      target_sources:
                        - a
                    case2:
                      name: case2
                      max_duration_seconds: 600
                      target_sources:
                        - d
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetSrcMatchesTwoCases(db_path)

    def test_multihost_no_types_wildcard_regex_two_suites(self):
        """
        Test multihost support without types, with a DB-level wildcard regex,
        and two suites.
        """
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                  cases:
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      target_sources:
                        - a
                    case2:
                      name: case2
                      max_duration_seconds: 600
                      target_sources:
                        - d
                  host_types: .*
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetSrcMatchesTwoCases(db_path)
