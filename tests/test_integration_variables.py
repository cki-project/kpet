# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Template variable integration tests"""
from textwrap import dedent

from tests.test_integration import IntegrationTests
from tests.test_integration import assets_mkdir
from tests.test_integration import kpet_run_generate
from tests.test_integration import kpet_variable_list

# index.yaml base
INDEX_YAML_BASE = """
    host_types:
        normal: {}
    recipesets:
        rcs1:
          - normal
    arches:
        - arch
    trees:
        tree: {}
    template: beaker.xml.j2
"""

# index.yaml with required template variables
INDEX_YAML_REQUIRED_VARIABLES = \
    INDEX_YAML_BASE + """
    variables:
        x:
            description: Default-type variable x
        y:
            description: String variable y
            type: str
        z:
            description: Boolean variable z
            type: bool
"""

# index.yaml with optional template variables
INDEX_YAML_OPTIONAL_VARIABLES = \
    INDEX_YAML_BASE + """
    variables:
        x:
            description: Default-type variable x
            default: DEFAULT_X
        y:
            description: String variable y
            type: str
            default: DEFAULT_Y
        z:
            description: Boolean variable z
            type: bool
            default: False
"""

# beaker.xml outputting all variables
BEAKER_XML = "{{ VARIABLES.x }} {{ VARIABLES.y }} {{ VARIABLES.z }}"

# Database outputting required variables
DB_REQUIRED_VARIABLES = {
    "index.yaml": INDEX_YAML_REQUIRED_VARIABLES,
    "beaker.xml.j2": BEAKER_XML
}

# Database outputting optional variables
DB_OPTIONAL_VARIABLES = {
    "index.yaml": INDEX_YAML_OPTIONAL_VARIABLES,
    "beaker.xml.j2": BEAKER_XML
}


class IntegrationVariablesTests(IntegrationTests):
    """Integration tests for template variable interface"""

    def test_short_opt(self):
        """Check short option is accepted"""
        with assets_mkdir(DB_REQUIRED_VARIABLES) as db_path:
            self.assertKpetProduces(kpet_run_generate, db_path,
                                    "-vx=X", "-vy=Y", "-vz=True",
                                    "--no-lint",
                                    stdout_equals='X Y True')

    def test_long_opt(self):
        """Check long option is accepted"""
        with assets_mkdir(DB_REQUIRED_VARIABLES) as db_path:
            self.assertKpetProduces(kpet_run_generate, db_path,
                                    "--variable", "x=X",
                                    "--variable", "y=Y",
                                    "--variable", "z=True",
                                    "--no-lint",
                                    stdout_equals='X Y True')

    def test_required_not_specified(self):
        """Check not specifying a required variable aborts rendering"""
        with assets_mkdir(DB_REQUIRED_VARIABLES) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=1,
                stderr_matching=r".*Required variables not set: "
                                r"'x', 'y', 'z'\n")

    def test_unknown_specified(self):
        """Check specifying an unknown variable aborts rendering"""
        with assets_mkdir(DB_REQUIRED_VARIABLES) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint", "-v", "a=VALUE", status=1,
                stderr_matching=r".*Unknown variable: 'a'\n")

    def test_invalid_value_specified(self):
        """Check specifying an invalid value aborts rendering"""
        with assets_mkdir(DB_REQUIRED_VARIABLES) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint", "-v", "z=FALSE", status=1,
                stderr_matching=r".*Invalid value 'FALSE' for "
                                r"variable 'z' of type 'bool'\n")

    def test_invalid_default_specified(self):
        """Check specifying an invalid default aborts loading"""
        with assets_mkdir({
                    "index.yaml": INDEX_YAML_REQUIRED_VARIABLES +
                    "            default: foobar",
                    "beaker.xml.j2": BEAKER_XML
                }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint", status=1,
                stderr_matching=r".*The default value 'foobar' of "
                                r"variable 'z' doesn't match its type "
                                r"'bool'\n")

    def test_optional_not_specified(self):
        """Check not specifying an optional variables outputs defaults"""
        with assets_mkdir(DB_OPTIONAL_VARIABLES) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_equals='DEFAULT_X DEFAULT_Y False')

    def test_optional_specified(self):
        """Check specifying an optional variable overrides default"""
        with assets_mkdir(DB_OPTIONAL_VARIABLES) as db_path:
            self.assertKpetProduces(kpet_run_generate, db_path,
                                    "-vx=VALUE_X",
                                    "-vy=VALUE_Y",
                                    "-vz=True",
                                    "--no-lint",
                                    stdout_equals='VALUE_X VALUE_Y True')

    def test_boolean_values(self):
        """Check boolean values are properly recognized/rejected"""
        with assets_mkdir(DB_OPTIONAL_VARIABLES) as db_path:
            for input_str, output_str in {
                "false": "False",
                "False": "False",
                "true": "True",
                "True": "True",
                "FALSE": None,
                "TRUE": None,
                "FOOBAR": None,
                "0": None,
                "1": None,
                "": None,
            }.items():
                if output_str is None:
                    self.assertKpetProduces(
                        kpet_run_generate, db_path,
                        f"-vz={input_str}",
                        "--no-lint", status=1,
                        stderr_matching=f".*Invalid value {input_str!r} "
                                        f"for variable 'z' of type 'bool'\n"
                    )
                else:
                    self.assertKpetProduces(
                        kpet_run_generate, db_path,
                        f"-vz={input_str}",
                        "--no-lint",
                        stdout_equals=f'DEFAULT_X DEFAULT_Y {output_str}'
                    )

    def test_list(self):
        """Check variables are properly listed"""
        with assets_mkdir(DB_OPTIONAL_VARIABLES) as db_path:
            self.assertKpetProduces(
                kpet_variable_list, db_path,
                stdout_equals=dedent("""\
                    x Default-type variable x
                      Default: DEFAULT_X
                      Type: str
                    y String variable y
                      Default: DEFAULT_Y
                      Type: str
                    z Boolean variable z
                      Default: False
                      Type: bool
                """)
            )
            self.assertKpetProduces(
                kpet_variable_list, db_path, ".*",
                stdout_equals=dedent("""\
                    x Default-type variable x
                      Default: DEFAULT_X
                      Type: str
                    y String variable y
                      Default: DEFAULT_Y
                      Type: str
                    z Boolean variable z
                      Default: False
                      Type: bool
                """)
            )
            self.assertKpetProduces(
                kpet_variable_list, db_path, "x",
                stdout_equals=dedent("""\
                    x Default-type variable x
                      Default: DEFAULT_X
                      Type: str
                """)
            )
            self.assertKpetProduces(
                kpet_variable_list, db_path, "x|z",
                stdout_equals=dedent("""\
                    x Default-type variable x
                      Default: DEFAULT_X
                      Type: str
                    z Boolean variable z
                      Default: False
                      Type: bool
                """)
            )
            self.assertKpetProduces(
                kpet_variable_list, db_path, "",
                stdout_equals=dedent("""\
                """)
            )

        with assets_mkdir(DB_REQUIRED_VARIABLES) as db_path:
            self.assertKpetProduces(
                kpet_variable_list, db_path,
                stdout_equals=dedent("""\
                    x Default-type variable x
                    y String variable y
                    z Boolean variable z
                """)
            )
            self.assertKpetProduces(
                kpet_variable_list, db_path, ".*",
                stdout_equals=dedent("""\
                    x Default-type variable x
                    y String variable y
                    z Boolean variable z
                """)
            )
            self.assertKpetProduces(
                kpet_variable_list, db_path, "x",
                stdout_equals=dedent("""\
                    x Default-type variable x
                """)
            )
            self.assertKpetProduces(
                kpet_variable_list, db_path, "x|z",
                stdout_equals=dedent("""\
                    x Default-type variable x
                    z Boolean variable z
                """)
            )
            self.assertKpetProduces(
                kpet_variable_list, db_path, "",
                stdout_equals=dedent("""\
                """)
            )
