# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration miscellaneous tests"""
from textwrap import dedent

from tests.test_integration import BEAKER_XML_J2
from tests.test_integration import INDEX_BASE_YAML
from tests.test_integration import IntegrationTests
from tests.test_integration import assets_mkdir
from tests.test_integration import kpet_run_generate
from tests.test_integration import kpet_run_test_list
from tests.test_integration import kpet_test_list
from tests.test_integration import kpet_with_db

# Sorry, please bear with us, pylint: disable=too-many-lines

class IntegrationMiscTests(IntegrationTests):
    # Calm down, pylint: disable=too-many-public-methods
    """Miscellaneous integration tests"""
    # It's OK pylint: disable=too-many-public-methods

    def test_empty_tree_list(self):
        """Test tree listing with empty database"""
        assets = {
            "index.yaml": """
                # Empty but valid database
                {}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_with_db, db_path,
                                    "-d", "tree", "list")

    def test_empty_run_generate(self):
        """Test run generation with empty database"""
        assets = {
            "index.yaml": """
                # Empty but valid database
                {}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                status=1,
                stderr_matching=r".*Tree 'tree' not found.*")

    def test_empty_run_test_list(self):
        """Test run test listing with empty database"""
        assets = {
            "index.yaml": """
                # Empty but valid database
                {}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                status=1,
                stderr_matching=r".*Tree 'tree' not found.*")

    def test_minimal_run_generate(self):
        """Test run generation with minimal database"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                    panicky:
                        ignore_panic: true
                    multihost_1: {}
                    multihost_2: {}
                recipesets:
                    rcs1:
                      - normal
                      - panicky
                    rcs2:
                      - multihost_1
                      - multihost_2

                # Minimal, but fully-functional database
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_run_generate, db_path,
                                    stdout_matching=r'.*<job>\s*</job>.*')

    def test_minimal_run_test_list(self):
        """Test run test listing with minimal database"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                    panicky:
                        ignore_panic: true
                    multihost_1: {}
                    multihost_2: {}
                recipesets:
                    rcs1:
                      - normal
                      - panicky
                    rcs2:
                      - multihost_1
                      - multihost_2

                # Minimal, but fully-functional database
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_run_test_list, db_path)

    def test_missing_template_run_generate(self):
        """Test run generation with a missing template"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                    panicky:
                        ignore_panic: true
                    multihost_1: {}
                    multihost_2: {}
                recipesets:
                    rcs1:
                      - normal
                      - panicky
                    rcs2:
                      - multihost_1
                      - multihost_2

                arches:
                    - arch
                trees:
                    tree: {}
                template: missing_template.xml.j2
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_run_generate,
                                    db_path,
                                    "-t", "tree",
                                    status=1,
                                    stderr_matching=r'.*TemplateNotFound.*')

    def test_missing_case_file_run_generate(self):
        """Test run generation with a missing case file"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                trees:
                    tree: {}
                template: beaker.xml.j2
                case: missing.yaml
            """,
            "beaker.xml.j2": """
                <job/>
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_run_generate,
                                    db_path,
                                    status=1,
                                    stderr_matching=r'.*missing.yaml.*')

    def test_invalid_top_yaml_tree_list(self):
        """Test tree listing with invalid YAML in the top database file"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                tree: {
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_with_db, db_path,
                                    "--debug", "tree", "list",
                                    status=1,
                                    stderr_matching=r'.*ParserError.*')

    def test_invalid_case_yaml_tree_list(self):
        """Test tree listing with invalid YAML in a case file"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                case: case.yaml
            """,
            "case.yaml": """
                {
                maintainers:
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_with_db, db_path,
                                    "--debug", "tree", "list",
                                    status=1,
                                    stderr_matching=r'.*ParserError.*')

    def test_invalid_top_data_tree_list(self):
        """Test tree listing with invalid data in the top database file"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                trees: {}
                unknown_node: True
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_with_db, db_path,
                                    "tree", "list",
                                    status=1,
                                    stderr_matching=r'.*Invalid database.*')

    def test_invalid_case_data_tree_list(self):
        """Test tree listing with invalid data in a case file"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                case: case.yaml
            """,
            "case.yaml": """
                name: "Case data with unknown nodes"
                foobar: True
                location: somewhere
                maintainers:
                  - name: maint1
                    email: maint1@maintainers.org
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_with_db, db_path,
                                    "tree", "list",
                                    status=1,
                                    stderr_matching=r'.*Invalid test case.*')

    def test_empty_abstract_case_run_generate(self):
        """Test run generation with an empty abstract case"""
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                case:
                  universal_id: test_uid
                  host_types: ^normal
                  cases: {}
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(kpet_run_generate, db_path,
                                    stdout_matching=r'.*<job>\s*</job>.*')

    def test_empty_case_no_patterns_run_generate(self):
        """Test run generation with an empty test case without patterns"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    A:
                      name: A
                      location: somewhere
                      maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*A - a\s*</job>.*')

    def test_empty_case_with_a_pattern_run_generate(self):
        """Test run generation with an empty test case with a pattern"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    A:
                      name: A
                      location: somewhere
                      maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*HOST\s*A - a\s*</job>.*')

    def test_preboot_tasks_are_added(self):
        """Test preboot tasks are added"""
        assets = {
            "index.yaml": """
                host_types:
                    normal:
                        preboot_tasks: preboot_tasks.xml.j2
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: beaker.xml.j2
                case:
                    universal_id: test_uid
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    max_duration_seconds: 600
                    host_types: ^normal
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
            "preboot_tasks.xml.j2": "<task>Some preboot task</task>"
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<task>\s*Some preboot task\s*</task>.*')

    def test_cases_expose_their_name(self):
        """Test case's "name" field should be exposed to Beaker templates"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    A:
                      name: case A
                      location: somewhere
                      cases:
                        a:
                          name: case a
                          max_duration_seconds: 600
                          maintainers:
                            - name: maint1
                              email: maint1@maintainers.org
            """,
            "beaker.xml.j2": """
            <job>
              {% for scene in SCENES %}
                {% for recipeset in scene.recipesets %}
                  {% for HOST in recipeset %}
                    {% for test in HOST.tests %}
                      {{ test.name }}
                    {% endfor %}
                  {% endfor %}
                {% endfor %}
              {% endfor %}
            </job>
            """,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>\s*case A - case a\s*</job>.*')

    def test_cases_expose_their_maintainers(self):
        """Test cases' "maintainers" field should be exposed to templates"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      maintainers:
                        - name: Some Maintainer
                          email: someone@maintainers.org
            """,
            "beaker.xml.j2": """
            <job>
              {% for scene in SCENES %}
                {% for recipeset in scene.recipesets %}
                  {% for HOST in recipeset %}
                    {% for test in HOST.tests %}
                      {{ test.name }}
                      Maintainers:
                      {% for m in test.maintainers %}
                        # {{ m.name | e }} {{ '<%s>' | format(m.email) | e }}
                      {% endfor %}
                    {% endfor %}
                  {% endfor %}
                {% endfor %}
              {% endfor %}
            </job>
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>.*case1.*'
                r'# Some Maintainer &lt;someone@maintainers.org&gt;.*</job>.*')

    def test_tests_argument(self):
        """Test matching with --tests argument"""
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    A:
                      name: A
                      location: somewhere
                      maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                      cases:
                        a:
                          name: a
                          max_duration_seconds: 600
                    B:
                      name: B
                      location: somewhere
                      maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                      cases:
                        b:
                          name: b
                          max_duration_seconds: 600
                        b2:
                          name: b2
                          max_duration_seconds: 600
            """,
            "beaker.xml.j2": BEAKER_XML_J2,
        }

        with assets_mkdir(assets) as db_path:
            test_name_re_output_re_map = {"B - b": r"HOST\s*B - b",
                                          "B.*": r"HOST\s*B - b\s*B - b2",
                                          "B": ""}
            for test_name_re, output_re in test_name_re_output_re_map.items():
                with self.subTest(test_name_re=test_name_re):
                    self.assertKpetProduces(
                        kpet_run_generate, db_path, "--tests", test_name_re,
                        stdout_matching=fr'.*'
                        fr'{output_re}'
                        fr'\s*</job>.*')

    def test_maintainer_struct(self):
        """Test cases' "maintainers" field should be exposed to templates"""
        # pylint: disable=line-too-long
        # flake8: noqa: E501
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      maintainers:
                        - name: Some Maintainer
                          email: someone@maintainers.org
                        - name: Other Maintainer
                          email: other@maintainers.org
                          gitlab: other
            """,
            "beaker.xml.j2": """
            <job>
              {% for scene in SCENES %}
                {% for recipeset in scene.recipesets %}
                  {% for HOST in recipeset %}
                    {% for test in HOST.tests %}
                      {{ test.name }}
                      Maintainers:
                      {% for m in test.maintainers %}
                        {{ '%s <%s> %s' | format(m.name, m.email, m.gitlab|d('NO_GL')) | e }}
                      {% endfor %}
                    {% endfor %}
                  {% endfor %}
                {% endfor %}
              {% endfor %}
            </job>
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                stdout_matching=r'.*<job>.*case1.*'
                r'.*Some Maintainer &lt;someone@maintainers.org&gt; NO_GL.*'
                r'.*Other Maintainer &lt;other@maintainers.org&gt; other.*'
                r'.*</job>.*')

    def test_invalid_maintainer(self):
        """
        Check that Object raiser Invalid exception if the schema doesn't match.
        """
        assets = {
            "index.yaml": INDEX_BASE_YAML + """
                    case1:
                      name: case1
                      max_duration_seconds: 600
                      maintainers:
                        - name: Some Maintainer
            """,
            "beaker.xml.j2": """
            <job>
              {% for scene in SCENES %}
                {% for recipeset in scene.recipesets %}
                  {% for HOST in recipeset %}
                    {% for test in HOST.tests %}
                      {{ test.name }}
                    {% endfor %}
                  {% endfor %}
                {% endfor %}
              {% endfor %}
            </job>
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_test_list, db_path, status=1,
                stderr_matching=r'.*Required member "email" is missing.*'
            )

    def test_global_template_present(self):
        """
        Check that global template specification is accepted
        """
        assets = {
            "index.yaml": """
                template: output.txt.j2
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                case:
                  universal_id: test_id
                  name: Test
                  host_types: normal
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
            """,
            "output.txt.j2": """
            {%- for scene in SCENES -%}
              {%- for recipeset in scene.recipesets -%}
                {%- for HOST in recipeset -%}
                  {%- for test in HOST.tests -%}
                    {{- test.name -}}
                  {%- endfor -%}
                {%- endfor -%}
              {%- endfor -%}
            {%- endfor -%}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=0,
                stdout_equals=r'Test')

    def test_global_template_missing(self):
        """
        Check that unspecified global template is accepted
        """
        assets = {
            "index.yaml": """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                case:
                  universal_id: test_id
                  name: Test
                  host_types: normal
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=0,
                stdout_equals=r'')

    def test_host_type_without_recipeset(self):
        """
        Check a host type without recipeset gets one created and assigned
        """
        assets = {
            "index.yaml": """
                template: output.txt.j2
                host_types:
                    a: {}
                    b: {}
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                case:
                  universal_id: test_uid
                  name: Test
                  host_types: b
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
            """,
            "output.txt.j2": """
            {%- for scene in SCENES -%}
              {%- for recipeset in scene.recipesets -%}
                {%- for HOST in recipeset -%}
                  {%- for test in HOST.tests -%}
                    {{- test.name -}}
                  {%- endfor -%}
                {%- endfor -%}
              {%- endfor -%}
            {%- endfor -%}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=0,
                stdout_equals=r'Test')

    def test_two_host_types_without_recipeset(self):
        """
        Check two host types without recipesets get an implicit one each
        """
        assets = {
            "index.yaml": """
                template: output.txt.j2
                host_types:
                    a: {}
                    b: {}
                    c: {}
                recipesets:
                    rcs1:
                      - a
                arches:
                    - arch
                trees:
                    tree: {}
                case:
                  universal_id: test_uid
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    test1:
                      name: Test1
                      host_types: b
                    test2:
                      name: Test2
                      host_types: c
            """,
            "output.txt.j2": """
            {%- for scene in SCENES -%}
              {%- for recipeset in scene.recipesets -%}
                {{- "RECIPESET:" -}}
                {%- for HOST in recipeset -%}
                  {{- "HOST:" -}}
                  {%- for test in HOST.tests -%}
                    {{- test.name + "\\n" -}}
                  {%- endfor -%}
                {%- endfor -%}
              {%- endfor -%}
            {%- endfor -%}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=0,
                stdout_equals='RECIPESET:HOST:Test1\n'
                              'RECIPESET:HOST:Test2\n')

    def test_empty_recipesets(self):
        """
        Check run generation works with empty "recipesets"
        """
        assets = {
            "index.yaml": """
                template: output.txt.j2
                host_types:
                    a: {}
                recipesets: {}
                arches:
                    - arch
                trees:
                    tree: {}
                case:
                  universal_id: test_uid
                  name: Test
                  host_types: a
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
            """,
            "output.txt.j2": """
            {%- for scene in SCENES -%}
              {%- for recipeset in scene.recipesets -%}
                {%- for HOST in recipeset -%}
                  {%- for test in HOST.tests -%}
                    {{- test.name -}}
                  {%- endfor -%}
                {%- endfor -%}
              {%- endfor -%}
            {%- endfor -%}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=0,
                stdout_equals='Test')

    def test_no_recipesets(self):
        """
        Check run generation works without "recipesets"
        """
        assets = {
            "index.yaml": """
                template: output.txt.j2
                host_types:
                    a: {}
                arches:
                    - arch
                trees:
                    tree: {}
                case:
                  universal_id: test_uid
                  name: Test
                  host_types: a
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
            """,
            "output.txt.j2": """
            {%- for scene in SCENES -%}
              {%- for recipeset in scene.recipesets -%}
                {%- for HOST in recipeset -%}
                  {%- for test in HOST.tests -%}
                    {{- test.name -}}
                  {%- endfor -%}
                {%- endfor -%}
              {%- endfor -%}
            {%- endfor -%}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=0,
                stdout_equals='Test')

    def test_no_recipesets_with_domains(self):
        """
        Check run generation works without "recipesets" and with domains
        """
        assets = {
            "index.yaml": """
                template: output.txt.j2
                domains:
                    general:
                        description: Generally-available hosts
                host_types:
                    a:
                        domains: general
                arches:
                    - arch
                trees:
                    tree: {}
                case:
                  universal_id: test_uid
                  name: Test
                  host_types: a
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
            """,
            "output.txt.j2": """
            {%- for scene in SCENES -%}
              {%- for recipeset in scene.recipesets -%}
                {%- for HOST in recipeset -%}
                  {%- for test in HOST.tests -%}
                    {{- test.name -}}
                  {%- endfor -%}
                {%- endfor -%}
              {%- endfor -%}
            {%- endfor -%}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=0,
                stdout_equals='Test')

    def test_empty_recipesets_with_domains(self):
        """
        Check run generation works with empty "recipesets" and with domains
        """
        assets = {
            "index.yaml": """
                template: output.txt.j2
                domains:
                    general:
                        description: Generally-available hosts
                host_types:
                    a:
                        domains: general
                recipesets: {}
                arches:
                    - arch
                trees:
                    tree: {}
                case:
                  universal_id: test_uid
                  name: Test
                  host_types: a
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
            """,
            "output.txt.j2": """
            {%- for scene in SCENES -%}
              {%- for recipeset in scene.recipesets -%}
                {%- for HOST in recipeset -%}
                  {%- for test in HOST.tests -%}
                    {{- test.name -}}
                  {%- endfor -%}
                {%- endfor -%}
              {%- endfor -%}
            {%- endfor -%}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=0,
                stdout_equals='Test')

    def test_run_with_multiple_executions(self):
        """
        Check run generation works with empty "recipesets" and with domains
        """
        assets = {
            "index.yaml": """
                template: output.txt.j2
                domains:
                  A:
                    description: A hosts
                  B:
                    description: B hosts
                host_types:
                  a:
                    domains: A
                  b:
                    domains: B
                recipesets: {}
                arches:
                  - arch1
                  - arch2
                trees:
                  tree1: {}
                  tree2: {}
                case:
                  universal_id: test_uid
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    a:
                      name: a
                      host_types: a
                    b:
                      name: b
                      host_types: b
            """,
            "output.txt.j2": """
            {%- for scene in SCENES -%}
              {{- "SCENE<" +
                  scene.arch + "," +
                  scene.tree + "," +
                  scene.kernel +
                  ">:" -}}
              {%- for recipeset in scene.recipesets -%}
                {%- for HOST in recipeset -%}
                  {%- for test in HOST.tests -%}
                    {{- test.name -}}
                  {%- endfor -%}
                {%- endfor -%}
              {%- endfor -%}
            {%- endfor -%}
            """,
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "-aarch1", "-ttree1", "--domains=A", "-kkernel1", "-e",
                "-aarch2", "-ttree2", "--domains=B", "-kkernel2",
                status=0,
                stdout_equals='SCENE<arch1,tree1,kernel1>:a'
                              'SCENE<arch2,tree2,kernel2>:b')

    def test_run_test_list_targeted(self):
        """
        Check run test listing handles --targeted option correctly
        """
        assets = {
            "index.yaml": """
                template: output.txt.j2
                host_types:
                  general: {}
                recipesets: {}
                arches:
                  - arch
                trees:
                  tree: {}
                case:
                  universal_id: test_uid
                  location: somewhere
                  host_types: general
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    untargeted:
                      name: Untargeted
                    targeted:
                      name: Targeted
                      target_sources: x.c
                    high_cost_untargeted:
                      name: High-cost untargeted
                      high_cost: true
                    high_cost_targeted:
                      name: High-cost targeted
                      high_cost: true
                      target_sources: x.c
            """,
            "output.txt.j2": "",
            "x.patch": dedent("""\
                diff --git a/x.c b/x.c
                new file mode 100644
                index 0000000..e69de29
            """),
            "unknown.patch": dedent("""\
                diff --git a/unknown.c b/unknown.c
                new file mode 100644
                index 0000000..e69de29
            """),
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "-aarch", "-ttree",
                status=0,
                stdout_equals=dedent("""\
                    High-cost untargeted
                    Targeted
                    Untargeted
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "-aarch", "-ttree", f"{db_path}/x.patch",
                status=0,
                stdout_equals=dedent("""\
                    High-cost targeted
                    High-cost untargeted
                    Targeted
                    Untargeted
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "-aarch", "-ttree", f"{db_path}/unknown.patch",
                status=0,
                stdout_equals=dedent("""\
                    High-cost untargeted
                    Untargeted
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "-aarch", "-ttree",
                "--targeted",
                status=0,
                stdout_equals=dedent("""\
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "-aarch", "-ttree", f"{db_path}/x.patch",
                "--targeted",
                status=0,
                stdout_equals=dedent("""\
                    High-cost targeted
                    Targeted
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "-aarch", "-ttree", f"{db_path}/unknown.patch",
                "--targeted",
                status=0,
                stdout_equals=""
            )

    def test_host_types(self):
        """
        Check case's host_types is handled correctly
        """
        assets = {
            "index.yaml": """
                template: output.txt.j2
                arches:
                  - arch1
                  - arch2
                host_types:
                  arch1:
                    supported:
                        arches: arch1
                  arch2:
                    supported:
                        arches: arch2
                trees:
                  tree: {}
                case:
                  universal_id: test_uid
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    arch1:
                      name: arch1
                      host_types: arch1
                    arch2:
                      name: arch2
                      host_types: arch2
                    both:
                      name: both
                      host_types: arch1|arch2
            """,
            "output.txt.j2": "",
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "-aarch1", "-ttree",
                status=0,
                stdout_equals=dedent("""\
                    arch1
                    both
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "-aarch2", "-ttree",
                status=0,
                stdout_equals=dedent("""\
                    arch2
                    both
                """)
            )

    def test_not_sources(self):
        """
        Check not_sources is handled correctly
        """
        test_patch = dedent("""
                diff --git a/file1 b/file1
                new file mode 100644
                index 0000000..e69de29
                diff --git a/file2 b/file2
                new file mode 100644
                index 0000000..e69de29
                diff --git a/file3 b/file3
                new file mode 100644
                index 0000000..e69de29
                diff --git a/file4 b/file4
                new file mode 100644
                index 0000000..e69de29
            """)
        index_yaml = """
                template: output.txt.j2
                arches:
                  - arch
                trees:
                  tree: {}
                host_types:
                  normal: {}
                case:
                  universal_id: test
                  location: somewhere
                  maintainers:
                    - name: maint
                      email: maint@maintainers.org
                  cases:
                    file1:
                      name: File1
                      target_sources: file1
                    file2:
                      name: File2
                      target_sources: file2
                    file3:
                      name: File3
                      target_sources: file3
        """
        assets = {
            "index.yaml": index_yaml,
            "output.txt.j2": "",
            "test.patch": test_patch
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_with_db, db_path, "patch", "list-files",
                db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    file1
                    file2
                    file3
                    file4
                """)
            )
            self.assertKpetProduces(
                kpet_with_db, db_path, "patch", "list-files", "--sources",
                db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    file1
                    file2
                    file3
                    file4
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path, "--targeted",
                status=0,
                stdout_equals=dedent("""\
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path, "--targeted",
                "--", db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )

        assets = {
            "index.yaml": index_yaml + """
                not_sources: file4
            """,
            "output.txt.j2": "",
            "test.patch": test_patch
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_with_db, db_path, "patch", "list-files",
                db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    file1
                    file2
                    file3
                    file4
                """)
            )
            self.assertKpetProduces(
                kpet_with_db, db_path, "patch", "list-files", "--sources",
                db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    file1
                    file2
                    file3
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path, "--targeted",
                status=0,
                stdout_equals=dedent("""\
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path, "--targeted",
                "--", db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )

        assets = {
            "index.yaml": index_yaml + """
                not_sources: '.*'
            """,
            "output.txt.j2": "",
            "test.patch": test_patch
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_with_db, db_path, "patch", "list-files",
                db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    file1
                    file2
                    file3
                    file4
                """)
            )
            self.assertKpetProduces(
                kpet_with_db, db_path, "patch", "list-files", "--sources",
                db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    file1
                    file2
                    file3
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path, "--targeted",
                "--", db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )

        assets = {
            "index.yaml": index_yaml + """
                not_sources: file3
            """,
            "output.txt.j2": "",
            "test.patch": test_patch
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_with_db, db_path, "patch", "list-files",
                db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    file1
                    file2
                    file3
                    file4
                """)
            )
            self.assertKpetProduces(
                kpet_with_db, db_path, "patch", "list-files", "--sources",
                db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    file1
                    file2
                    file3
                    file4
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path, "--targeted",
                "--", db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                db_path + "/test.patch",
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )

    def test_file(self):
        """
        Check -f/--file/--file-list options are handled correctly
        """
        index_yaml = """
                template: output.txt.j2
                arches:
                  - arch
                trees:
                  tree: {}
                host_types:
                  normal: {}
                case:
                  universal_id: test
                  location: somewhere
                  maintainers:
                    - name: maint
                      email: maint@maintainers.org
                  cases:
                    file1:
                      name: File1
                      target_sources: file1
                    file2:
                      name: File2
                      target_sources: file2
                    file3:
                      name: File3
                      target_sources: file3
        """
        assets = {
            "index.yaml": index_yaml,
            "output.txt.j2": "",
            "file-list.txt": "file1\nfile2\n",
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "-ffile1",
                status=0,
                stdout_equals=dedent("""\
                    File1
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "--file=file1",
                status=0,
                stdout_equals=dedent("""\
                    File1
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "-ffile1", "-ffile1",
                status=0,
                stdout_equals=dedent("""\
                    File1
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "-ffile1", "-ffile2",
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-ffile1",
                status=0,
                stdout_equals=dedent("""\
                    File1
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "--file=file1",
                status=0,
                stdout_equals=dedent("""\
                    File1
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-ffile1", "-ffile1",
                status=0,
                stdout_equals=dedent("""\
                    File1
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-ffile1", "-ffile2",
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "--file-list", db_path + "/file-list.txt",
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                """)
            )

        assets = {
            "index.yaml": index_yaml + """
                not_sources: file1
            """,
            "output.txt.j2": "",
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "-ffile1",
                status=0,
                stdout_equals=dedent("""\
                    File1
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "-ffile1", "-ffile1",
                status=0,
                stdout_equals=dedent("""\
                    File1
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "-ffile1", "-ffile2",
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                    File3
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-ffile1",
                status=0,
                stdout_equals=dedent("""\
                    File1
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "--file=file1",
                status=0,
                stdout_equals=dedent("""\
                    File1
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-ffile1", "-ffile1",
                status=0,
                stdout_equals=dedent("""\
                    File1
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-ffile1", "-ffile2",
                status=0,
                stdout_equals=dedent("""\
                    File1
                    File2
                """)
            )

    def test_empty_mbox(self):
        """
        Check that supplying an empty mailbox counts as no-sources
        """
        non_empty_patch = dedent("""
                diff --git a/file1 b/file1
                new file mode 100644
                index 0000000..e69de29
            """)
        index_yaml = """
                template: output.txt.j2
                arches:
                  - arch
                trees:
                  tree: {}
                host_types:
                  normal: {}
                case:
                  universal_id: test
                  location: somewhere
                  maintainers:
                    - name: maint
                      email: maint@maintainers.org
                  cases:
                    high-cost_mistargeted:
                      name: High-cost mistargeted
                      high_cost: true
                      target_sources: file2
                    high-cost_targeted:
                      name: High-cost targeted
                      high_cost: true
                      target_sources: file1
                    high-cost_untargeted:
                      name: High-cost untargeted
                      high_cost: true
                    mistargeted:
                      name: Mistargeted
                      target_sources: file2
                    targeted:
                      name: Targeted
                      target_sources: file1
                    untargeted:
                      name: Untargeted
        """
        assets = {
            "index.yaml": index_yaml,
            "output.txt.j2": "",
            "non_empty.patch": non_empty_patch,
            "empty.patch": "",
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                status=0,
                stdout_equals=dedent("""\
                    High-cost untargeted
                    Mistargeted
                    Targeted
                    Untargeted
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "--high-cost=no",
                status=0,
                stdout_equals=dedent("""\
                    Mistargeted
                    Targeted
                    Untargeted
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "--high-cost=targeted",
                status=0,
                stdout_equals=dedent("""\
                    High-cost untargeted
                    Mistargeted
                    Targeted
                    Untargeted
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                "--high-cost=yes",
                status=0,
                stdout_equals=dedent("""\
                    High-cost mistargeted
                    High-cost targeted
                    High-cost untargeted
                    Mistargeted
                    Targeted
                    Untargeted
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                db_path + "/empty.patch",
                status=0,
                stdout_equals=dedent("""\
                    High-cost untargeted
                    Untargeted
                """)
            )
            self.assertKpetProduces(
                kpet_run_test_list, db_path,
                db_path + "/non_empty.patch",
                status=0,
                stdout_equals=dedent("""\
                    High-cost targeted
                    High-cost untargeted
                    Targeted
                    Untargeted
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                status=0,
                stdout_equals=dedent("""\
                    High-cost mistargeted
                    High-cost targeted
                    High-cost untargeted
                    Mistargeted
                    Targeted
                    Untargeted
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                db_path + "/empty.patch",
                status=0,
                stdout_equals=dedent("""\
                    High-cost untargeted
                    Untargeted
                """)
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                db_path + "/non_empty.patch",
                status=0,
                stdout_equals=dedent("""\
                    High-cost targeted
                    High-cost untargeted
                    Targeted
                    Untargeted
                """)
            )

    def test_schemas(self):
        """
        Check that various output schemas are accepted and handled correctly.
        """
        base_index_yaml = """
                template: output.xml.j2
                arches:
                  - arch
                trees:
                  tree: {}
                host_types:
                  normal: {}
                case:
                  universal_id: test
                  location: somewhere
                  maintainers:
                    - name: maint
                      email: maint@maintainers.org
        """
        output_xml_j2 = "<?xml version='1.0' encoding='utf-8'?>\n<job/>\n"

        # No schemas
        with assets_mkdir({
            "index.yaml": base_index_yaml,
            "output.xml.j2": output_xml_j2,
        }) as db_path:
            # Lint on
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                status=0, stdout_matching=".*",
            )
            # Lint off
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                status=0, stdout_matching=".*",
            )

        # Empty list of schemas
        with assets_mkdir({
            "index.yaml": base_index_yaml + """
                schemas: []
            """,
            "output.xml.j2": output_xml_j2,
        }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                status=0, stdout_matching=".*",
            )

        # Single XSD schema matching the doc
        with assets_mkdir({
            "index.yaml": base_index_yaml + """
                schemas: schema.xsd
            """,
            "output.xml.j2": output_xml_j2,
            "schema.xsd": """
                <schema xmlns="http://www.w3.org/2001/XMLSchema">
                    <element name="job">
                        <complexType/>
                    </element>
                </schema>
            """,
        }) as db_path:
            # Lint on
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                status=0, stdout_matching=".*",
            )
            # Lint off
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                status=0, stdout_matching=".*",
            )

        # Single XSD schema mismatching the doc
        with assets_mkdir({
            "index.yaml": base_index_yaml + """
                schemas: schema.xsd
            """,
            "output.xml.j2": output_xml_j2,
            "schema.xsd": """
                <schema xmlns="http://www.w3.org/2001/XMLSchema">
                    <element name="not_job">
                        <complexType/>
                    </element>
                </schema>
            """,
        }) as db_path:
            # Lint on
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                status=1,
                stderr_matching=".*Element 'job': No matching global "
                "declaration available for the validation root.*",
            )
            # Lint off
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                status=0, stdout_matching=".*",
            )

        # Single-item schema list mismatching the doc
        with assets_mkdir({
            "index.yaml": base_index_yaml + """
                schemas:
                    - schema.xsd
            """,
            "output.xml.j2": output_xml_j2,
            "schema.xsd": """
                <schema xmlns="http://www.w3.org/2001/XMLSchema">
                    <element name="not_job">
                        <complexType/>
                    </element>
                </schema>
            """,
        }) as db_path:
            # Lint on
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                status=1,
                stderr_matching=".*Element 'job': No matching global "
                "declaration available for the validation root.*",
            )
            # Lint off
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                status=0, stdout_matching=".*",
            )

        # Single RelaxNG schema matching the doc
        with assets_mkdir({
            "index.yaml": base_index_yaml + """
                schemas: schema.rng
            """,
            "output.xml.j2": output_xml_j2,
            "schema.rng": """
                <element name="job" xmlns="http://relaxng.org/ns/structure/1.0">
                    <empty/>
                </element>
            """,
        }) as db_path:
            # Lint on
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                status=0, stdout_matching=".*",
            )
            # Lint off
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                status=0, stdout_matching=".*",
            )

        # Single RelaxNG schema mismatching the doc
        with assets_mkdir({
            "index.yaml": base_index_yaml + """
                schemas: schema.rng
            """,
            "output.xml.j2": output_xml_j2,
            "schema.rng": """
                <element name="not_job" xmlns="http://relaxng.org/ns/structure/1.0">
                    <empty/>
                </element>
            """,
        }) as db_path:
            # Lint on
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                status=1,
                stderr_matching=".*Expecting element not_job, got job.*",
            )
            # Lint off
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                status=0, stdout_matching=".*",
            )

        # Single Schematron schema matching the doc
        with assets_mkdir({
            "index.yaml": base_index_yaml + """
                schemas: schema.sch
            """,
            "output.xml.j2": output_xml_j2,
            "schema.sch": """
                <schema xmlns="http://purl.oclc.org/dsdl/schematron">
                  <pattern>
                    <rule context="/">
                      <assert test="job">
                        Job not found!
                      </assert>
                    </rule>
                  </pattern>
                </schema>
            """,
        }) as db_path:
            # Lint on
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                status=0, stdout_matching=".*",
            )
            # Lint off
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                status=0, stdout_matching=".*",
            )

        # Single Schematron schema mismatching the doc
        with assets_mkdir({
            "index.yaml": base_index_yaml + """
                schemas: schema.sch
            """,
            "output.xml.j2": output_xml_j2,
            "schema.sch": """
                <schema xmlns="http://purl.oclc.org/dsdl/schematron">
                  <pattern>
                    <rule context="/">
                      <assert test="not_job">
                        Not-job not found!
                      </assert>
                    </rule>
                  </pattern>
                </schema>
            """,
        }) as db_path:
            # Lint on
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                status=1,
                stderr_matching=".*Not-job not found!.*",
            )
            # Lint off
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                status=0, stdout_matching=".*",
            )

        # Two XSD schemas matching the doc
        with assets_mkdir({
            "index.yaml": base_index_yaml + """
                schemas:
                    - schema.xsd
                    - schema.xsd
            """,
            "output.xml.j2": output_xml_j2,
            "schema.xsd": """
                <schema xmlns="http://www.w3.org/2001/XMLSchema">
                    <element name="job">
                        <complexType/>
                    </element>
                </schema>
            """,
        }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                status=0, stdout_matching=".*",
            )

        # First XSD schema mismatching the doc
        with assets_mkdir({
            "index.yaml": base_index_yaml + """
                schemas:
                    - schema_bad.xsd
                    - schema_good.xsd
            """,
            "output.xml.j2": output_xml_j2,
            "schema_good.xsd": """
                <schema xmlns="http://www.w3.org/2001/XMLSchema">
                    <element name="job">
                        <complexType/>
                    </element>
                </schema>
            """,
            "schema_bad.xsd": """
                <schema xmlns="http://www.w3.org/2001/XMLSchema">
                    <element name="not_job">
                        <complexType/>
                    </element>
                </schema>
            """,
        }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                status=1,
                stderr_matching=".*Element 'job': No matching global "
                "declaration available for the validation root.*",
            )

        # Second XSD schema mismatching the doc
        with assets_mkdir({
            "index.yaml": base_index_yaml + """
                schemas:
                    - schema_good.xsd
                    - schema_bad.xsd
            """,
            "output.xml.j2": output_xml_j2,
            "schema_good.xsd": """
                <schema xmlns="http://www.w3.org/2001/XMLSchema">
                    <element name="job">
                        <complexType/>
                    </element>
                </schema>
            """,
            "schema_bad.xsd": """
                <schema xmlns="http://www.w3.org/2001/XMLSchema">
                    <element name="not_job">
                        <complexType/>
                    </element>
                </schema>
            """,
        }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                status=1,
                stderr_matching=".*Element 'job': No matching global "
                "declaration available for the validation root.*",
            )

        # XSD, RelaxNG, and Schematron schemas matching the doc
        with assets_mkdir({
            "index.yaml": base_index_yaml + """
                schemas:
                    - schema.xsd
                    - schema.rng
                    - schema.sch
            """,
            "output.xml.j2": output_xml_j2,
            "schema.xsd": """
                <schema xmlns="http://www.w3.org/2001/XMLSchema">
                    <element name="job">
                        <complexType/>
                    </element>
                </schema>
            """,
            "schema.rng": """
                <element name="job" xmlns="http://relaxng.org/ns/structure/1.0">
                    <empty/>
                </element>
            """,
            "schema.sch": """
                <schema xmlns="http://purl.oclc.org/dsdl/schematron">
                  <pattern>
                    <rule context="/">
                      <assert test="job">
                        Job not found!
                      </assert>
                    </rule>
                  </pattern>
                </schema>
            """,
        }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                status=0, stdout_matching=".*",
            )

        # XSD, RelaxNG, and Schematron schemas mismatching the doc
        with assets_mkdir({
            "index.yaml": base_index_yaml + """
                schemas:
                    - schema.xsd
                    - schema.rng
                    - schema.sch
            """,
            "output.xml.j2": output_xml_j2,
            "schema.xsd": """
                <schema xmlns="http://www.w3.org/2001/XMLSchema">
                    <element name="not_job">
                        <complexType/>
                    </element>
                </schema>
            """,
            "schema.rng": """
                <element name="not_job" xmlns="http://relaxng.org/ns/structure/1.0">
                    <empty/>
                </element>
            """,
            "schema.sch": """
                <schema xmlns="http://purl.oclc.org/dsdl/schematron">
                  <pattern>
                    <rule context="/">
                      <assert test="not_job">
                        Not-job not found!
                      </assert>
                    </rule>
                  </pattern>
                </schema>
            """,
        }) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                status=1,
                stderr_matching=".*Element 'job': No matching global "
                "declaration available for the validation root.*",
            )

    def test_host_type_description(self):
        """
        Check a host type description is accepted and exposed to templates
        """
        assets = {
            "index.yaml": """
                template: output.txt.j2
                host_types:
                    a:
                        description: A host
                    b: {}
                arches:
                    - arch
                trees:
                    tree: {}
                case:
                  universal_id: test_uid
                  location: somewhere
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    a:
                      name: Test A
                      host_types: a
                    b:
                      name: Test B
                      host_types: b
            """,
            "output.txt.j2": dedent("""\
            {%- for scene in SCENES -%}
              {%- for recipeset in scene.recipesets -%}
                {%- for HOST in recipeset -%}
                    {{- "DESCRIPTION: " +
                        (HOST.type_description | default('NONE', true)) +
                        "\\n" -}}
                {%- endfor -%}
              {%- endfor -%}
            {%- endfor -%}
            """),
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", status=0,
                stdout_equals='DESCRIPTION: A host\nDESCRIPTION: NONE\n')
